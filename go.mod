module gitlab.com/infra.run/public/b3scale

go 1.14

require (
	github.com/Masterminds/squirrel v1.5.0
	github.com/go-redis/redis/v8 v8.2.3
	github.com/google/uuid v1.1.2
	github.com/jackc/pgx/v4 v4.10.0
	github.com/labstack/echo-contrib v0.9.0
	github.com/labstack/echo/v4 v4.1.17
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/prometheus/client_golang v1.8.0 // indirect
	github.com/prometheus/common v0.15.0 // indirect
	github.com/rs/zerolog v1.20.0
	github.com/urfave/cli/v2 v2.3.0
	github.com/ziflex/lecho/v2 v2.1.0
	golang.org/x/crypto v0.0.0-20201203163018-be400aefbc4c // indirect
	golang.org/x/net v0.0.0-20201202161906-c7110b5ffcbb
	golang.org/x/sys v0.0.0-20201204225414-ed752295db88 // indirect
	golang.org/x/text v0.3.4 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)
